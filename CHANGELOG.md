# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/) and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## 0.3.0

### Added

- Nextcloud 26 support
- Occitan translation

## 0.2.1

### Fixed

- Bumped version number

## 0.2.0

### Added

- Support for Nextcloud 25
- Added two new languages (German and French) for the Netherlands holidays data
- Dutch translation
- French changelog

### Changed

- Added date span next to country selection in holidays country selector

### Removed

- Support for Nextcloud 24

## 0.1.0

### Added

- First release
