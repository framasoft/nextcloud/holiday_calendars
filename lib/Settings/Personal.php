<?php
/**
 * @copyright Copyright (c) 2022 Thomas Citharel <nextcloud@tcit.fr>
 *
 * @author Thomas Citharel <nextcloud@tcit.fr>
 *
 * @license GNU AGPL version 3 or any later version
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

namespace OCA\HolidayCalendars\Settings;

use OCA\HolidayCalendars\AppInfo\Application;
use OCP\AppFramework\Http\TemplateResponse;
use OCP\AppFramework\Services\IInitialState;
use OCP\Settings\ISettings;
use OCP\Util;

class Personal implements ISettings {
	private IInitialState $initialState;

	public function __construct(IInitialState $initialState) {
		$this->initialState = $initialState;
	}

	public function getForm(): TemplateResponse {
		/** @psalm-var array */
		$holidayCalendars = json_decode(file_get_contents(__DIR__ . '/../../data/holiday_calendars.json'));
		/** @psalm-var array */
		$religiousCalendars = json_decode(file_get_contents(__DIR__ . '/../../data/religious_calendars.json'));
		/** @psalm-var array */
		$schoolHolidays = json_decode(file_get_contents(__DIR__ . '/../../data/school_holidays.json'));

		Util::addScript(Application::APP_ID, 'holiday_calendars-personalSettings');
		Util::addStyle(Application::APP_ID, 'personal');
		$this->initialState->provideInitialState('holiday_calendars', $holidayCalendars);
		$this->initialState->provideInitialState('religious_calendars', $religiousCalendars);
		$this->initialState->provideInitialState('school_holidays', $schoolHolidays);

		return new TemplateResponse(Application::APP_ID, 'personal');
	}

	public function getSection(): string {
		return 'calendar';
	}

	/**
	 * @psalm-return 40
	 */
	public function getPriority(): int {
		return 40;
	}
}
