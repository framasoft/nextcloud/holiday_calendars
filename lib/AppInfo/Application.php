<?php

namespace OCA\HolidayCalendars\AppInfo;

use OCP\AppFramework\App;

class Application extends App {
	public const APP_ID = 'holiday_calendars';

	public function __construct() {
		parent::__construct(self::APP_ID);
	}
}
