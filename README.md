# Nextcloud Holiday Calendars

An app listing links to public calendars representing the holidays and allowing to subscribe to them.

## Settings 
    
By going to **Settings > Personal > Groupware** you can subscribe to the different calendars.

![](screenshots/list_subscriptions.png)
![](screenshots/add_new_subscription.png)
