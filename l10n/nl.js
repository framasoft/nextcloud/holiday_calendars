OC.L10N.register(
    "holiday_calendars",
    {
    "Public holidays in {country}" : "Feestdagen in {land}",
    "Calendar holidays" : "Kalendervakanties",
    "Easily subscribe to external calendars like public holidays." : "Abonneer u eenvoudig op externe agenda's zoals feestdagen.",
    "Add subscription" : "Abonnement toevoegen",
    "No public holidays calendar subscriptions" : "Geen abonnementen voor publieke feestdagen",
    "Public holidays" : "Feestdagen",
    "Pick a country" : "Kies een land",
    "Religious calendars" : "Religieuze kalenders",
    "Pick a subscription" : "Kies een abonnement",
    "School vacations" : "Schoolvakanties",
    "Sources" : "Bronnen",
    "Public holiday calendar list provided by {thunderbirdLink}" : "Lijst met feestdagenkalender geleverd door {thunderbirdLink}",
    "Religious calendar list provided by:" : "Religieuze kalenderlijst geleverd door:",
    "We can't allow adding their calendars directly, but you may find some interesting things to subscribe to on the following websites. Once the feed URL copied, you can subscribe to it directly into the Calendar App." : "We kunnen niet toestaan dat hun agenda's rechtstreeks worden toegevoegd, maar op de volgende websites kunt u interessante dingen vinden om u op te abonneren. Nadat de feed-URL is gekopieerd, kunt u zich er rechtstreeks op abonneren in de Agenda App.",
    "Holiday Calendars" : "Vakantiekalenders",
    "Allows to subscribe to holidays public calendars" : "Maakt het mogelijk om je te abonneren op openbare feestdagenkalenders",
    "This is an app listing links to public calendars representing the holidays and allowing to subscribe to them" : "Dit is een app met links naar openbare agenda's die feestdagen vertegenwoordigen en waarmee je je erop kunt abonneren"
},
"nplurals=2; plural=n != 1;");
