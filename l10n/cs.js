OC.L10N.register(
    "holiday_calendars",
    {
    "Public holidays in {country}" : "Státní svátky v {country}",
    "Calendar holidays" : "Kalendář státních svátků",
    "Easily subscribe to external calendars like public holidays." : "Snadno se přihlaste k odběru externích kalendářů, jako jsou státní svátky.",
    "Add subscription" : "Přidat přihlášení se k odběru",
    "No public holidays calendar subscriptions" : "Žádná přihlášení se k odběru kalendářů se státními svátky",
    "Public holidays" : "Státní svátky",
    "Pick a country" : "Vyberte stát",
    "Religious calendars" : "Kalendáře náboženských svátků",
    "Pick a subscription" : "Vyberte přihlášení se k odběru",
    "School vacations" : "Školní prázdniny",
    "Sources" : "Zdroje",
    "Public holiday calendar list provided by {thunderbirdLink}" : "Seznam kalendářů se státními svátky poskytován {thunderbirdLink}",
    "Religious calendar list provided by:" : "Seznam kalendářů s náboženskými svátky poskytován:",
    "We can't allow adding their calendars directly, but you may find some interesting things to subscribe to on the following websites. Once the feed URL copied, you can subscribe to it directly into the Calendar App." : "Nemůžeme umožnit přidávání jejich kalendářů přímo, ale na následujících webech můžete nalézt zajímavé věci k odběru kterých se přihlásit. Po zkopírování URL adresy kanálu se k odběru můžete přihlásit přímo z aplikace Kalendář.",
    "Holiday Calendars" : "Kalendáře státních svátků",
    "Allows to subscribe to holidays public calendars" : "Umožňuje se přihlásit k odběru kalendářů se státními svátky",
    "This is an app listing links to public calendars representing the holidays and allowing to subscribe to them" : "Toto je aplikace vypisující odkazy na veřejné kalendáře představující svátky a umožňující se přihlásit se k odběru z nich"
},
"nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;");
