OC.L10N.register(
    "holiday_calendars",
    {
    "Holiday Calendars" : "Calendièr dels jorns de fèsta",
    "Allows to subscribe to holidays public calendars" : "Permet de s’abonar als calendièrs publics de jorns de fèsta",
    "This is an app listing links to public calendars representing the holidays and allowing to subscribe to them" : "Una aplicacion qu’amassa los ligams cap als calendièrs publics mostrant las fèstas que s’i podèm abonar",
    "Calendar holidays" : "Calendièr dels jorns de fèsta",
    "Easily subscribe to external calendars like public holidays." : "Abonatz-vos facilament als calendièrs extèrns.",
    "Add subscription" : "Apondre un abonament",
    "No public holidays calendar subscriptions" : "Cap d’abonament als calendièrs",
    "Public holidays" : "Jorn de fèsta",
    "Pick a country" : "Causissètz un país",
    "Religious calendars" : "Calendièrs religioses",
    "Pick a subscription" : "Causissètz un abonament",
    "School vacations" : "Vacanças escolaras",
    "Sources" : "Fonts",
    "Public holiday calendar list provided by {thunderbirdLink}" : "Lista dels calendièrs de jorn de fèsta provesida per {thunderbirdLink}",
    "Religious calendar list provided by:" : "Lista dels calendièrs religioses provesida per :",
    "We can't allow adding their calendars directly, but you may find some interesting things to subscribe to on the following websites. Once the feed URL copied, you can subscribe to it directly into the Calendar App." : "Podèm pas permetre l'apondon dirècte de lors calendièrs, mas podètz trobar de causas interessantas a las qualas vos abonar suls sits web seguents. Un còp l'URL del flux copiada, vos i podètz abonar dirèctament dins l'aplicacion Agenda.",
    "Public holidays in {country}" : "Jorn de fèsta en {country}"
},
"nplurals=2; plural=n > 1;");
